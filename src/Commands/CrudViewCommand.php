<?php

namespace gen11\CrudGenerator\Commands;

use File;
use Illuminate\Console\Command;

class CrudViewCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'crud:view
                            {name : The name of the Crud.}
                            {--fields= : The field names for the form.}
                            {--view-path= : The name of the view path.}
                            {--route-group= : Prefix of the route group.}
                            {--pk=id : The name of the primary key.}
                            {--validations= : Validation rules for the fields.}
                            {--form-helper=html : Helper for the form.}
                            {--file=filename : Helper for the form.}
                            {--custom-data= : Some additional values to use in the crud.}
                            {--localize=no : Localize the view? yes|no.}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create views for the Crud.';

    /**
     * View Directory Path.
     *
     * @var string
     */
    protected $viewDirectoryPath;
    protected $customViewDirectoryPath;

    /**
     *  Form field types collection.
     *
     * @var array
     */
    protected $typeLookup = [
        'string' => 'text',
        'char' => 'text',
        'varchar' => 'text',
        'text' => 'textarea',
        'mediumtext' => 'textarea',
        'longtext' => 'textarea',
        'json' => 'textarea',
        'jsonb' => 'textarea',
        'binary' => 'textarea',
        'password' => 'password',
        'email' => 'email',
        'number' => 'number',
        'integer' => 'number',
        'unsignedInteger' => 'number',
        'bigint' => 'number',
        'mediumint' => 'number',
        'tinyint' => 'number',
        'smallint' => 'number',
        'decimal' => 'float',
        'double' => 'float',
        'float' => 'float',
        'date' => 'date',
        'datetime' => 'datetime-local',
        'timestamp' => 'datetime-local',
        'time' => 'time',
        'radio' => 'radio',
        'boolean' => 'radio',
        'checkbox' => 'checkbox',
        'enum' => 'select',
        'select' => 'select',
        'file' => 'file',
        'header' => 'header',
        'paragraph' => 'paragraph',
        'separator' => 'separator',
        'embed' => 'embed',
        'feed' => 'feed',
        'pad' => 'pad',
        'checkfeed' => 'checkfeed',
        'tab' => 'tab',
        'tabpanel' => 'tabpanel',
        'listselect' => 'listselect',
        'filter'  => 'filter',
        'token'  => 'token',
        'script'  => 'script',
        'image'  => 'image',
        'staticimage'  => 'staticimage',
        'date'  => 'date',
        'carousel' => 'carousel',
        'slider' => 'slider',
        'label' => 'label',
        'readonly' => 'readonly',
        'html' => 'html'
    ];

    /**
     * Variables that can be used in stubs
     *
     * @var array
     */
    protected $vars = [
        'layout',
        'formFields',
        'formFieldsHtml',
        'scriptsJavascript',
        'varName',
        'crudName',
        'crudNameCap',
        'crudNameSingular',
        'primaryKey',
        'modelName',
        'modelNameCap',
        'viewName',
        'routePrefix',
        'routePrefixCap',
        'routeGroup',
        'formHeadingHtml',
        'formBodyHtml',
        'viewTemplateDir',
        'formBodyHtmlForShowView',
        'export',
        'pagination',
        'navigation',
        'search',
        'heading',
        'showButtonHtml',
        'editButtonHtml',
        'deleteButtonHtml',
        'filters_navigation'

    ];

    /**
     * Form's fields.
     *
     * @var array
     */
    protected $formFields = [];

    /**
     * Html of Form's fields.
     *
     * @var string
     */
    protected $formFieldsHtml = '';
    protected $scriptsJavascript = '';

    /**
     * Number of columns to show from the table. Others are hidden.
     *
     * @var integer
     */
    protected $defaultColumnsToShow = 5;

    /**
     * Variable name with first letter in lowercase
     *
     * @var string
     */
    protected $varName = '';

    /**
     * Name of the Crud.
     *
     * @var string
     */
    protected $crudName = '';

    /**
     * Crud Name in capital form.
     *
     * @var string
     */
    protected $crudNameCap = '';

    /**
     * Crud Name in singular form.
     *
     * @var string
     */
    protected $crudNameSingular = '';

    /**
     * Primary key of the model.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Name of the Model.
     *
     * @var string
     */
    protected $modelName = '';

    /**
     * Name of the Model with first letter in capital
     *
     * @var string
     */
    protected $modelNameCap = '';

    /**
     * Name of the View Dir.
     *
     * @var string
     */
    protected $viewName = '';

    /**
     * Prefix of the route
     *
     * @var string
     */
    protected $routePrefix = '';

    /**
     * Prefix of the route with first letter in capital letter
     *
     * @var string
     */
    protected $routePrefixCap = '';

    /**
     * Name or prefix of the Route Group.
     *
     * @var string
     */
    protected $routeGroup = '';

    protected $layout = '';


    /**
     * Html of the form heading.
     *
     * @var string
     */
    protected $formHeadingHtml = '<thead>
        <tr>
            <th class="text-center"><p class="cell-counter-hr">N.</p></th>';

    /**
     * Html of the form body.
     *
     * @var string
     */
    protected $formBodyHtml = '';
    protected $showButtonHtml = '';
    protected $editButtonHtml = '';
    protected $deleteButtonHtml = '';


    /**
     * Html of view to show.
     *
     * @var string
     */
    protected $formBodyHtmlForShowView = '';

    /**
     * User defined values
     *
     * @var array
     */
    protected $customData = [];

    /**
     * Template directory where views are generated
     *
     * @var string
     */
    protected $viewTemplateDir = '';

    protected $heading = '';
    /**
     * Delimiter used for replacing values
     *
     * @var array
     */
    protected $delimiter;

    /**
     * Create a new command instance.
     *
     */


    public function __construct()
    {
        parent::__construct();

        if (config('crudgenerator.view_columns_number')) {
            $this->defaultColumnsToShow = config('crudgenerator.view_columns_number');
        }

        $this->delimiter = config('crudgenerator.custom_delimiter')
            ? config('crudgenerator.custom_delimiter')
            : ['%%', '%%'];



    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {

        $formHelper = $this->option('form-helper');
        $crudxmlfilename = $this->option('file');
        $json = File::get($crudxmlfilename);
        $jsonfile = json_decode($json);
        $this->jsonfile = $jsonfile;
        $this->layout = "app";
        $this->show_status = false;
        $this->show_creationdate = false;
        $this->export_type = "xls";
        $this->pagination_type = "both";
        $this->has_search = "yes";
        $this->has_heading = true;

        $this->edit_button = "icon";
        $this->delete_button = "icon";
        $this->show_button = "icon";

        if(isset($jsonfile->main)) {
          if(isset($jsonfile->main->layout)) $this->layout = $jsonfile->main->layout;
          if(isset($jsonfile->main->show_status)) $this->show_status = ($jsonfile->main->show_status == "yes") ? true : false;
          if(isset($jsonfile->main->export)) $this->export_type = $jsonfile->main->export;
          if(isset($jsonfile->main->pagination)) $this->pagination_type = $jsonfile->main->pagination;
          if(isset($jsonfile->main->show_creationdate)) $this->show_creationdate = ($jsonfile->main->show_creationdate == "yes") ? true : false;
          if(isset($jsonfile->main->search)) $this->has_search = $jsonfile->main->search;
          if(isset($jsonfile->main->heading)) $this->has_heading = ($jsonfile->main->heading == "yes") ? true : false;
          if(isset($jsonfile->main->edit)) $this->edit_button = $jsonfile->main->edit;
          if(isset($jsonfile->main->delete)) $this->delete_button = $jsonfile->main->delete;
          if(isset($jsonfile->main->show)) $this->show_button = $jsonfile->main->show;
        }

        $this->viewDirectoryPath = config('crudgenerator.custom_template')
            ? config('crudgenerator.path') . 'views/' . $formHelper . '/'
            : __DIR__ . '/../stubs/views/' . $formHelper . '/';




        $this->crudName = strtolower($this->argument('name'));
        $this->varName = lcfirst($this->argument('name'));
        $this->crudNameCap = ucwords($this->crudName);
        $this->crudNameSingular = str_singular($this->crudName);
        $this->modelName = str_singular($this->argument('name'));
        $this->modelNameCap = ucfirst($this->modelName);
        $this->customData = $this->option('custom-data');
        $this->primaryKey = $this->option('pk');
        $this->routeGroup = ($this->option('route-group'))
            ? $this->option('route-group') . '/'
            : $this->option('route-group');
        $this->routePrefix = ($this->option('route-group')) ? $this->option('route-group') : '';
        $this->routePrefixCap = ucfirst($this->routePrefix);
        $this->viewName = snake_case($this->argument('name'), '-');

        $this->customViewDirectoryPath = config('crudgenerator.custom_template')
            ? config('crudgenerator.path') . 'views/' . $this->viewName . '/'
            : __DIR__ . '/../stubs/views/' . $this->viewName . '/';

        if($this->edit_button == "icon") {

          $this->editButtonHtml = $this->fillStub('tools/edit-button-stub.html',
              array(
                'routeGroup' => $this->routeGroup,
                'viewName' => $this->viewName,
                'primaryKey' => $this->primaryKey,
                'modelName' => $this->modelName
              )
          );
        }

        if($this->delete_button == "icon") {

          $this->deleteButtonHtml = $this->fillStub('tools/delete-button-stub.html',
              array(
                'routeGroup' => $this->routeGroup,
                'viewName' => $this->viewName,
                'primaryKey' => $this->primaryKey,
                'modelName' => $this->modelName
              )
          );
        }

        if($this->show_button == "icon") {

          $this->showButtonHtml = $this->fillStub('tools/show-button-stub.html',
              array(
                'routeGroup' => $this->routeGroup,
                'viewName' => $this->viewName,
                'primaryKey' => $this->primaryKey,
                'modelName' => $this->modelName
              )
          );
        }


// BOTTONE EXPORT IN XLS

        $this->export = "";
        if($this->export_type == "xls") {

              $this->export = $this->fillStub('tools/export-stub.html',
                  array(
                    'crudNameSingular' => $this->crudNameSingular
                  )
              );
        }


// CONTROLLI PAGINAZIONE

        $this->pagination = "";
        $this->navigation = "";

        if($this->pagination_type == "both") {

              $this->pagination = $this->fillStub('tools/pagination-stub.html',
                  array(
                    'crudNameSingular' => $this->crudNameSingular
                  )
              );

              $this->navigation = $this->fillStub('tools/navigation-stub.html',
                  array(
                    'crudNameSingular' => $this->crudNameSingular
                  )
              );
        }

// RICERCA

        $this->search = "";
        if($this->has_search == "yes") {

              $this->search = $this->fillStub('tools/search-stub.html',
                  array(
                    'crudNameSingular' => $this->crudNameSingular
                  )
              );

      }


        $viewDirectory = config('view.paths')[0] . '/';
        if ($this->option('view-path')) {
            $this->userViewPath = $this->option('view-path');
            $path = $viewDirectory . $this->userViewPath . '/' . $this->viewName . '/';
        } else {
            $path = $viewDirectory . $this->viewName . '/';
        }



        $this->viewTemplateDir = isset($this->userViewPath)
            ? $this->userViewPath . '.' . $this->viewName
            : $this->viewName;

        if (!File::isDirectory($path)) {
            File::makeDirectory($path, 0755, true);
        }

        $fields = $this->option('fields');
        $fieldsArray = explode(';', $fields);

        $this->formFields = [];

        $validations = $this->option('validations');



        if ($fields) {
            $x = 0;
            foreach ($fieldsArray as $item) {

                $itemArray = explode('#', $item);

                $this->formFields[$x]['name'] = trim($itemArray[0]);
                $this->formFields[$x]['type'] = trim($itemArray[1]);
                $this->formFields[$x]['required'] = preg_match('/' . $itemArray[0] . '/', $validations) ? true : false;

                $optionalFields = count($itemArray) - 2;

                if($optionalFields > 0)
                  for($z = 0; $z < $optionalFields; $z++) {

                    if(isset($itemArray[$z + 2])) {
                      $toprocess = explode('=', $itemArray[$z + 2]);

                      $this->formFields[$x][$toprocess[0]] = $toprocess[1];
                    }
                }

                $x++;
            }
        }


        $this->filters_navigation = "";

        foreach ($this->formFields as $item) {

          if($item['type'] == "filter") {

            if($this->filters_navigation == '') {
              $this->filters_navigation = ".'?";
            }
            else {
              $this->filters_navigation .= ".'&";
            }

            $this->filters_navigation .= $item['name']."='.\$".$item['name'];

            echo $this->filters_navigation."\n\n";

          }


          if(File::exists($this->viewDirectoryPath . 'form-fields/'.$item['type'].'-stub.js')) {

                $params =   array(
                    'itemName' => $item['name'],
                    'crudNameSingular' => $this->crudNameSingular,
                    'viewName' => $this->viewName,
                    'routeGroup' => $this->routeGroup


                );

                if($item['type'] == "carousel") {

                  $titleName = 'name';
                  $carousel_n = 3; // default value
                /*  $slide_script = '<img src="/uploads/<?php echo rtrim("product_id", "_id"); ?>/picture/{{$item->picture}}">';
*/
                  if(isset($item['extra'])) {
                      $extra = explode('|', $item['extra']);
                      foreach($extra as $ext) {
                        if(substr($ext, 0, 9 ) === "carousel:") {
                          $carousel_n = explode(':', $ext)[1];
                        }
                /*        if(substr($ext, 0, 6 ) === "slide:") {
                          $scriptName = explode(':', $ext)[1];
                          $slide_script = $jsonfile->main->$scriptName;
                        }
                        if(substr($ext, 0, 6 ) === "title:") {
                          $titleName = explode(':', $ext)[1];
                        }
*/
                      }
                  }

                  $params['size'] = $carousel_n;
            //      $params['slide'] = $slide_script;
            //      $params['title'] = $titleName;


                }

                  echo $item['type'].'-stub.js';

                  $this->scriptsJavascript .= $this->fillStub('form-fields/'.$item['type'].'-stub.js',
                    $params
                  );
          }

          if(isset($item['extra'])) {
            $extra = explode('|', $item['extra']);
          } else {
            $extra = [];
          }

          if(isset($item['data'])) {
            $datainfo = explode('|', $item['data']);
            if(count($datainfo) > 3) {
              $filter = explode(':', $datainfo[3]);
              //$filter = explode(':', $datainfo[3]);
            //  $filter[0] = $datainfo[3];
            //  $filter[1] = $datainfo[4];
              $this->scriptsJavascript .= $this->fillStub('form-fields/feed-stub-additional.js',
                  array(
                    'filter' => $filter[1],
                    'viewName' => $this->viewName,
                    'itemName' => $item['name']
                  )
              );

            }


          }


          if(!in_array("hidden", $extra)) {

            $this->formFieldsHtml .= $this->createField($item);

          }
          else {
            $this->formFieldsHtml .= $this->createHiddenField($item);

          }
        }

        $i = 0;
        foreach ($this->formFields as $key => $value) {

        //    if ($i == $this->defaultColumnsToShow) {
        //        break;
        //    }
        $field = $value['name'];
        $label = ucwords(str_replace('_', ' ', $field));

        $partformBodyHtmlForShowView = '<tr><th> ' . $label . ' </th><td> {{ $%%crudNameSingular%%->' . $field . ' }} </td></tr>';

        if($value['type'] == 'separator')
          $partformBodyHtmlForShowView = '';



            if(isset($value['extra'])) {

                        $extra = explode('|', $value['extra']);

                      //  if(!in_array("hidden", $extra)) {
                        if(in_array("index", $extra)) {
                              $this->formHeadingHtml .= '<th>@sortablelink(\''.$field.'\', trans(\'' . $this->viewName . '.' . $field . '\'))</th>';
                              if(in_array("lookup", $extra)) {
                                $this->formBodyHtml .= '<td>{{ trans(\''.$this->viewName.'.\'.$item->' . $field . ') }}</td>';
                              }
                              else
                              if($value['type'] == 'feed') {
                                $data = explode('|', $value['data']);

                                $this->formBodyHtml .= '<td>{{ $item->' . $data[0] . '->'.$data[2].' }}</td>';
                              }
                              else
                              if($value['type'] == 'script') {


                                $scriptName = $value['name'];
                                //echo "SCRIPT: ". $value['data'].$jsonfile->main->$scriptName."\n\n";

                                $this->formBodyHtml .= '<td>'.$jsonfile->main->$scriptName.'</td>';
                              }
                              else {
                                $this->formBodyHtml .= '<td>{{ $item->' . $field . ' }}</td>';
                              }
                          }
                    //    }
                          if(in_array("hidden", $extra)) {
                            $partformBodyHtmlForShowView = '';
                          }


                        $i++;

            }

            $this->formBodyHtmlForShowView .= $partformBodyHtmlForShowView;

        }

        if($this->show_creationdate) {
          $this->formHeadingHtml .= '<th>@sortablelink(\'created_at\', trans(\'' . $this->crudNameSingular . '.created_at\'))</th>';
          $this->formBodyHtml .= '<td>{{ date_format($item->created_at, "d-m-Y") }}</td>';
        }

        if($this->show_status) {
          $this->formHeadingHtml .= '<th>@sortablelink(\'status\', trans(\'' . $this->crudNameSingular . '.status\'))</th>';
          $this->formBodyHtml .= '<td align="center"><i class="fa fa-circle status-{{ ($item->status) ? $item->status : "null"}}" aria-hidden="true"></td>';
        }

        $this->formHeadingHtml = $this->formHeadingHtml.
        '<th></th>
    </tr>
</thead>';

        if(!$this->has_heading)
            $this->formHeadingHtml = '';

        $this->templateStubs($path);

        $this->info('View created successfully.');
    }

    /**
     * Default template configuration if not provided
     *
     * @return array
     */
    private function defaultTemplating()
    {
        return [
          'index' => ['filters_navigation', 'showButtonHtml', 'editButtonHtml', 'deleteButtonHtml', 'heading','export', 'pagination', 'navigation', 'search', 'crudNameSingular', 'layout', 'formHeadingHtml', 'formBodyHtml', 'crudName', 'crudNameCap', 'modelName', 'viewName', 'routeGroup', 'primaryKey'],
          'form' => ['crudNameSingular', 'viewName', 'modelName', 'layout', 'formFieldsHtml', 'scriptsJavascript', 'routeGroup'],
          'create' => ['filters_navigation', 'crudNameSingular', 'layout', 'crudName', 'crudNameCap', 'modelName', 'modelNameCap', 'viewName', 'routeGroup', 'viewTemplateDir'],
          'edit' => ['filters_navigation', 'layout', 'crudName', 'crudNameSingular', 'crudNameCap', 'modelNameCap', 'modelName', 'viewName', 'routeGroup', 'primaryKey', 'viewTemplateDir'],
          'show' => ['filters_navigation', 'layout', 'formHeadingHtml', 'formBodyHtml', 'formBodyHtmlForShowView', 'crudName', 'crudNameSingular', 'crudNameCap', 'modelName', 'viewName', 'routeGroup', 'primaryKey'],
          ];
    }

    /**
     * Generate files from stub
     *
     * @param $path
     */
    protected function templateStubs($path)
    {
        $dynamicViewTemplate = config('crudgenerator.dynamic_view_template')
            ? config('crudgenerator.dynamic_view_template')
            : $this->defaultTemplating();

        foreach ($dynamicViewTemplate as $name => $vars) {


            if(File::Exists($this->customViewDirectoryPath . $name . '.blade.stub')) {
              echo "FOUND CUSTOM TEMPLATE FOR VIEW: ".$this->customViewDirectoryPath . $name."\n\n";
              $file = $this->customViewDirectoryPath . $name . '.blade.stub';
            } else {
              $file = $this->viewDirectoryPath . $name . '.blade.stub';
            }

            $newFile = $path . $name . '.blade.php';
            if (!File::copy($file, $newFile)) {
                echo "failed to copy $file...\n";
            } else {
                $this->templateVars($newFile, $vars);
                $this->userDefinedVars($newFile);
            }
        }
    }

    /**
     * Update specified values between delimiter with real values
     *
     * @param $file
     * @param $vars
     */
    protected function templateVars($file, $vars)
    {
        $start = $this->delimiter[0];
        $end = $this->delimiter[1];

        foreach ($vars as $var) {
            $replace = $start . $var . $end;
            if (in_array($var, $this->vars)) {
                //  echo "REPLACINGX: ".$replace." ".$this->$var."\n";
                File::put($file, str_replace($replace, $this->$var, File::get($file)));
            }
        }
    }

    /**
     * Update custom values between delimiter with real values
     *
     * @param $file
     */
    protected function userDefinedVars($file)
    {
        $start = $this->delimiter[0];
        $end = $this->delimiter[1];

        if ($this->customData !== null) {
            $customVars = explode(';', $this->customData);
            foreach ($customVars as $rawVar) {
                $arrayVar = explode('=', $rawVar);
            //    echo "REPLACING: ".$replace." ".$this->$var;
                File::put($file, str_replace($start . $arrayVar[0] . $end, $arrayVar[1], File::get($file)));
            }
        }
    }

    /**
     * Form field wrapper.
     *
     * @param  string $item
     * @param  string $field
     *
     * @return string
     */
    protected function wrapField($item, $field)
    {
        $formGroup = File::get($this->viewDirectoryPath . 'form-fields/wrap-stub.html');

        $labelText = "'" . ucwords(strtolower(str_replace('_', ' ', $item['name']))) . "'";

        if ($this->option('localize') == 'yes') {
            $labelText = 'trans(\'' . $this->crudName . '.' . $item['name'] . '\')';
        }

        $tooltipA = '';
        $tooltipB = '';

        if(isset($item['extra'])) {
            $extra = explode('|', $item['extra']);

            if(in_array("tooltip", $extra)) {
              $tooltipA = '<i class="fa fa-info '.$item['type'].'_tooltip_icon"></i>';
              $tooltipB = 'data-toggle="tooltip" data-placement="top" title="{{trans(\'' . $this->crudName . '.' . $item['name'] . '_tooltip'.'\')}}"';
            }
        }

        $class = (isset($item['class']) ? $item['class'] : '');





      return sprintf($formGroup, $item['name'], $labelText, $field, $class, $tooltipA, $tooltipB);
    }

    /**
     * Form field generator.
     *
     * @param  array $item
     *
     * @return string
     */
    protected function createField($item)
    {
        switch ($this->typeLookup[$item['type']]) {
            case 'password':
                return $this->createPasswordField($item);
            case 'datetime-local':
            case 'readonly':
                return $this->createReadOnlyField($item);
            case 'html':
                    return $this->createHTMLField($item);
            case 'time':
                return $this->createInputField($item);
            case 'radio':
                return $this->createRadioFieldAG($item);
            case 'checkbox':
                return $this->createCheckboxField($item);
            case 'feed':
                return $this->createFeed($item);
            case 'pad':
                return $this->createPad($item);
            case 'carousel':
                    return $this->createCarousel($item);
            case 'slider':
                    return $this->createSlider($item);
            case 'checkfeed':
                return $this->createCheckFeed($item);
            case 'locator':
                return $this->createLocator($item);
            case 'header':
                  return $this->createHeader($item);
            case 'tab':
                  return $this->createTab($item);
            case 'tabpanel':
                  return $this->createTabPanel($item);
            case 'paragraph':
                  return $this->createParagraph($item);
            case 'separator':
                  return $this->createSeparator($item);
                  case 'float':
                        return $this->createFloatField($item);
            case 'embed':
                  return $this->createEmbed($item);
            case 'textarea':
                return $this->createTextareaField($item);
            case 'select':
            case 'enum':
                return $this->createSelectField($item);
            case 'listselect':
                return $this->createListSelectField($item);
            case 'filter':
                return $this->createFilterField($item);

                case 'token':
                    return $this->createTokenField($item);
            case 'script':
                //      return $this->createScript($item);
                return '';
            case 'file':
                return $this->createFileField($item);
            case 'image':
                return $this->createImageField($item);
            case 'staticimage':
                    return $this->createStaticImageField($item);

                    case 'label':
                            return $this->createLabelField($item);

                case 'date':
                    return $this->createDateField($item);
            default:
                return $this->createFormField($item);
        }
    }

    /**
     * Create a specific field using the form helper.
     *
     * @param  array $item
     *
     * @return string
     */
    protected function createFormField($item)
    {

        return $this->fillStubForItemAndWrap('form-fields/form-stub.html', $item, array());

/*
        $start = $this->delimiter[0];
        $end = $this->delimiter[1];

        $required = $item['required'] ? 'required' : '';

        $markup = File::get($this->viewDirectoryPath . 'form-fields/form-stub.html');
        $markup = str_replace($start . 'required' . $end, $required, $markup);
        $markup = str_replace($start . 'fieldType' . $end, $this->typeLookup[$item['type']], $markup);
        $markup = str_replace($start . 'itemName' . $end, $item['name'], $markup);
        $markup = str_replace($start . 'crudNameSingular' . $end, $this->crudNameSingular, $markup);

        return $this->wrapField(
            $item,
            $markup
        );*/
    }

    protected function createFileField($item)
    {


        return $this->fillStubForItemAndWrap('form-fields/file-stub.html', $item, array());
        /*
          array(
            'required' => $item['required'] ? 'required' : '',
            'fieldType' => $this->typeLookup[$item['type']],
            'itemName' => $item['name'],
            'crudNameSingular' => $this->crudNameSingular
          )
        );

        $start = $this->delimiter[0];
        $end = $this->delimiter[1];

        $required = $item['required'] ? 'required' : '';

        $markup = File::get($this->viewDirectoryPath . 'form-fields/file-stub.html');
        $markup = str_replace($start . 'required' . $end, $required, $markup);
        $markup = str_replace($start . 'fieldType' . $end, $this->typeLookup[$item['type']], $markup);
        $markup = str_replace($start . 'itemName' . $end, $item['name'], $markup);
        $markup = str_replace($start . 'crudNameSingular' . $end, $this->crudNameSingular, $markup);


        return $this->wrapField(
            $item,
            $markup
        );*/
    }


    protected function createImageField($item)
    {

      return $this->fillStubForItemAndWrap('form-fields/image-stub.html', $item, array());

    }

    protected function createStaticImageField($item)
    {

      return $this->fillStubForItemAndWrap('form-fields/staticimage-stub.html', $item, array());

    }

    protected function createLabelField($item)
    {

      return $this->fillStubForItemAndWrap('form-fields/label-stub.html', $item, array());

    }


    protected function createDateField($item)
    {

      return $this->fillStubForItemAndWrap('form-fields/date-stub.html', $item, array());

    }


    //protected $noSectionsYet = true;

    protected function createHeader($item)
    {

        $class = (isset($item['class']) ? $item['class'] : '');
        $class .= " ".$this->viewName;
        $pre = '';
        if(count($this->currentTab) > 0) {
            $this->currentTab = array();
            $pre = '
 </div>

</div> <!-- End tab content -->

';
        }

        return $pre.'
        </div>

<!-- '.$item['name'].'
---------------------------------------------------->

   <div class="row stdformbox '.$class.'" id="'.$item['name'].'">
    <div class="form-group col-xs-12 col-md-12 col-lg-12"><h3>{{ trans(\''.$this->viewName.'.'.$item['name'].'\')}}</h3></div>

';

    }

    protected $currentTab = array();


    protected function createTab($item)
    {
        $start = $this->delimiter[0];
        $end = $this->delimiter[1];

        $required = $item['required'] ? 'required' : '';

        $markup = File::get($this->viewDirectoryPath . 'form-fields/tab-stub.html');
        $markup = str_replace($start . 'options' . $end, $item['options'], $markup);

        $i = 0;
        foreach (json_decode($item['options'], true) as $optionKey => $optionValue) {
            $this->currentTab[$i++] = $optionKey;
        }

        $markup = str_replace($start . 'itemName' . $end, $item['name'], $markup);
        $markup = str_replace($start . 'itemName' . $end, $item['name'], $markup);
        $markup = str_replace($start . 'crudNameSingular' . $end, $this->crudNameSingular, $markup);

        $class = (isset($item['class']) ? $item['class'] : 'stdformbox');

        return '</div>

<!-- TAB '.$item['name'].' WITH OPTIONS '.$item['options'].'
---------------------------------------------------->

<div class="row stdformbox">
  <div class="form-group col-xs-12 col-md-12 col-lg-12">'.$markup.'

';

    }


    protected function createTabPanel($item)
    {
      /*
        $start = $this->delimiter[0];
        $end = $this->delimiter[1];

        $required = $item['required'] ? 'required' : '';

        $markup = File::get($this->viewDirectoryPath . 'form-fields/tabpanel.blade.stub');
        $markup = str_replace($start . 'itemName' . $end, $item['name'], $markup);
        $markup = str_replace($start . 'crudNameSingular' . $end, $this->crudNameSingular, $markup);

        echo  $item['name']." ".$item['class']."\n";
*/
        $class = (isset($item['class']) ? $item['class'] : 'stdformbox');

    /*    if($this->noSectionsYet) {
          $this->noSectionsYet = false;
          return '<div class="form-group col-xs-12 col-md-12 col-lg-12">'.$markup.'</div>';

        }
*/

    //  print_r($this->currentTab);

        $pre = '';
        if (($key = array_search($item['name'], $this->currentTab)) !== false) {
          //  print_r($this->currentTab)."\n";
            //unset($this->currentTab[$key]);
            if($key > 0)
              $pre = "
 </div>";
            else
              $pre= '<div class="tab-content col-12 pt-3">

';
        }


        return $pre .'

<!-- TAB PANEL '.$item['name'].'
------------------------------------->

 <div class="row tab-pane fade '.$class.'" id="'.$item['name'].'" role="tabpanel" aria-labelledby="'.$item['name'].'-tab">

';

  /*
        return '</div><div class="row '.$class.'" id="'.$item['name'].'" >

            <div class="form-group col-xs-12 col-md-12 col-lg-12">'.$markup.'</div>

            ';

        return $this->wrapField(
            $item,
            $markup
        ); */

    }


    protected function createParagraph($item)
    {

      return $this->fillStubForItemAndWrap('form-fields/paragraph-stub.html', $item, array());
      /*
      $markup = $this->fillStub('form-fields/paragraph-stub.html',
        array(
          'required' => $item['required'] ? 'required' : '',
          'itemName' => $item['name'],
          'crudNameSingular' => $this->crudNameSingular
        )
      );

        $start = $this->delimiter[0];
        $end = $this->delimiter[1];

        $required = $item['required'] ? 'required' : '';

        $markup = File::get($this->viewDirectoryPath . 'form-fields/paragraph.blade.stub');
        $markup = str_replace($start . 'itemName' . $end, $item['name'], $markup);
        $markup = str_replace($start . 'crudNameSingular' . $end, $this->crudNameSingular, $markup);

        return $this->wrapField(
            $item,
            $markup
        );*/
    }


    protected function createSeparator($item)
    {

        $class = (isset($item['class']) ? $item['class'] : 'stdformbox');
        $pre = '';
        if(count($this->currentTab) > 0) {
            $this->currentTab = array();
            $pre = '
 </div>

</div> <!-- End tab content -->

';
        }

        return $pre.'
        </div>

<!-- '.$item['name'].'
---------------------------------------------------->

   <div class="row col-12">
    <div class="form-group col-xs-12 col-md-12 col-lg-12"><hr></div>

';

    }


    protected function createEmbed($item)
    {
      $class = (isset($item['class']) ? $item['class'] : 'stdformbox');
      $pre = '';
      if(count($this->currentTab) > 0) {
          $this->currentTab = array();
          $pre = '
    </div>

    </div> <!-- End tab content -->

    ';
      }


      return '
        <iframe id="iframe_'.$item['name'].'" class="embeddedform '.$class.'" src="/'.$item['name'].'?token={{$token}}" scrolling="no" onload="this.style.height = this.contentWindow.document.body.scrollHeight + \'px\';"></iframe>
      ';

/* originale      return '</div>
      <div class="row greenformbox">
        <iframe class="embeddedform" src="/'.$item['name'].'/<?php if(isset($'.$this->crudNameSingular.'->id)) echo "list"; else echo "create"; ?>/{{$filter}}" scrolling="no" onload="this.style.height = this.contentWindow.document.body.scrollHeight + \'px\';"></iframe>
      ';
 */
    }

    /**
     * Create a password field using the form helper.
     *
     * @param  array $item
     *
     * @return string
     */
    protected function createPasswordField($item)
    {
        $required = $item['required'] ? 'required' : '';

        $required = ''; //PATCH PERCHé NON LEGGEVA CORRETTAMENTE  I required

        $markup = $this->fillStub('form-fields/password-stub.html',
          array(
            'required' => $required,
            'itemName' => $item['name'],
            'crudNameSingular' => $this->crudNameSingular
          )
        );
      /*
        $start = $this->delimiter[0];
        $end = $this->delimiter[1];





        $markup = File::get($this->viewDirectoryPath . 'form-fields/password-stub.html');
        $markup = str_replace($start . 'required' . $end, $required, $markup);
        $markup = str_replace($start . 'itemName' . $end, $item['name'], $markup);
        $markup = str_replace($start . 'crudNameSingular' . $end, $this->crudNameSingular, $markup);
*/

        return $this->wrapField(
            $item,
            $markup
        );
    }

    /**
     * Create a generic input field using the form helper.
     *
     * @param  array $item
     *
     * @return string
     */
    protected function createInputField($item)
    {

        return $this->fillStubForItemAndWrap('form-fields/input-stub.html', $item, array());

    }

    protected function createFloatField($item)
    {

        return $this->fillStubForItemAndWrap('form-fields/float-stub.html', $item, array());

    }

    protected function createTextAreaField($item)
    {

        return $this->fillStubForItemAndWrap('form-fields/textarea-stub.html', $item, array());

    }


    protected function createReadOnlyField($item)
    {

        return $this->fillStubForItemAndWrap('form-fields/readonly-stub.html', $item, array());

    }

    protected function createHiddenField($item)
    {

      return "{!! Form::hidden('".$item['name']."', null) !!}";

    }

    protected function createFilterField($item)
    {

      return "{!! Form::hidden('".$item['name']."', $".$item['name'].") !!}";

/*
        return "FILTER: ".$item['name'].
               "<br>VALUE: {{\$".$item['name']."}}{!! Form::hidden('".$item['name']."', $".$item['name'].") !!}";
*/
    }

    protected function createTokenField($item)
    {


      return "{!! Form::hidden('".$item['name']."', $".$item['name'].") !!}";

  /*
        return "FILTER: ".$item['name'].
               "<br>VALUE: {{\$".$item['name']."}}{!! Form::hidden('".$item['name']."', $".$item['name'].") !!}";
  */
    }


    /**
     * Create a yes/no radio button group using the form helper.
     *
     * @param  array $item
     *
     * @return string
     */

     /*
    protected function createRadioField($item)
    {
      $markup = $this->fillStub('form-fields/radio-stub.html',
        array(
          'required' => $item['required'] ? 'required' : '',
          'itemName' => $item['name'],
          'crudNameSingular' => $this->crudNameSingular
        )
      );

        return $this->wrapField(
            $item,
            $markup
        );
    }
*/
    /**
     * Create a textarea field using the form helper.
     *
     * @param  array $item
     *
     * @return string
     */
    protected function createScript($item)
    {
      $script_name = $item['name'];

      return $this->fillStubForItemAndWrap('form-fields/script-stub.html', $item,
      array(
        'body' => $this->jsonfile->main->$script_name
      ));

    }

    protected function createHTMLField($item)
    {
      return $this->fillStubForItemAndWrap('form-fields/html-stub.html', $item, array());

    }

    /**
     * Create a select field using the form helper.
     *
     * @param  array $item
     *
     * @return string
     */
    protected function createSelectField($item)
    {

      return $this->fillStubForItemAndWrap('form-fields/select-stub.html', $item, array('options' => $item['options']));

      /*
      $markup = $this->fillStub('form-fields/select-stub.html',
        array(
          'required' => $item['required'] ? 'required' : '',
          ,
          'itemName' => $item['name'],
          'crudNameSingular' => $this->crudNameSingular
        )
      );

        $start = $this->delimiter[0];
        $end = $this->delimiter[1];

        $required = $item['required'] ? 'required' : '';

        $markup = File::get($this->viewDirectoryPath . 'form-fields/select-stub.html');
        $markup = str_replace($start . 'required' . $end, $required, $markup);
        $markup = str_replace($start . 'options' . $end, $item['options'], $markup);
        $markup = str_replace($start . 'itemName' . $end, $item['name'], $markup);
        $markup = str_replace($start . 'crudNameSingular' . $end, $this->crudNameSingular, $markup);

        return $this->wrapField(
            $item,
            $markup
        );*/
    }

    protected function createListSelectField($item)
    {

      return $this->fillStubForItemAndWrap('form-fields/listselect-stub.html', $item,

      array(
        'options' => $item['options'],
        'itemSelectName' => $item['name'].'_select',
        'itemTextName' => $item['name'].'_text'
      ));


    }


    protected function createRadioFieldAG($item)
    {
      return $this->fillStubForItemAndWrap('form-fields/radio-stub.html', $item, array('options' => $item['options']));


/*
      $markup = $this->fillStub('form-fields/radio-stub.html',
        array(
          'required' => $item['required'] ? 'required' : '',
          'options' => $item['options'],
          'itemName' => $item['name'],
          'crudNameSingular' => $this->crudNameSingular
        )
      );


        $start = $this->delimiter[0];
        $end = $this->delimiter[1];

        $required = $item['required'] ? 'required' : '';


        $markup = File::get($this->viewDirectoryPath . 'form-fields/radio-stub.html');
        $markup = str_replace($start . 'required' . $end, $required, $markup);
        $markup = str_replace($start . 'options' . $end, $item['options'], $markup);
        $markup = str_replace($start . 'itemName' . $end, $item['name'], $markup);
        $markup = str_replace($start . 'crudNameSingular' . $end, $this->crudNameSingular, $markup);

        return $this->wrapField(
            $item,
            $markup
        );
*/    }

    protected function createCheckboxField($item)
    {

      return $this->fillStubForItemAndWrap('form-fields/checkbox-stub.html', $item, array('options' => $item['options']));
/*
      $markup = $this->fillStub('form-fields/checkbox-stub.html',
        array(
          'required' => $item['required'] ? 'required' : '',
          'options' => $item['options'],
          'itemName' => $item['name'],
          'crudNameSingular' => $this->crudNameSingular
        )
      );

        $start = $this->delimiter[0];
        $end = $this->delimiter[1];

        $required = $item['required'] ? 'required' : '';

        $markup = File::get($this->viewDirectoryPath . 'form-fields/checkbox-stub.html');
        $markup = str_replace($start . 'required' . $end, $required, $markup);
        $markup = str_replace($start . 'options' . $end, $item['options'], $markup);
        $markup = str_replace($start . 'itemName' . $end, $item['name'], $markup);
        $markup = str_replace($start . 'crudNameSingular' . $end, $this->crudNameSingular, $markup);

        return $this->wrapField(
            $item,
            $markup
        );*/
    }


    protected function createFeed($item)
    {
      return $this->fillStubForItemAndWrap('form-fields/feed-stub.html', $item, array());
    }

    protected function createPad($item)
    {
      return $this->fillStubForItemAndWrap('form-fields/pad-stub.html', $item, array());
    }

    protected function createCarousel($item)
    {
        $slide_script = '<img src="/uploads/<?php echo str_replace("_", "", rtrim("'.$item['name'].'", "_id")); ?>/picture/{{$item->picture}}">';
        $title = 'name';
        $description = 'name';
      //  echo $slide_script;

        if(isset($item['extra'])) {
            $extra = explode('|', $item['extra']);
            foreach($extra as $ext) {
              if(substr($ext, 0, 6 ) === "slide:") {
                $scriptName = explode(':', $ext)[1];
                $slide_script = $this->jsonfile->main->$scriptName;
              }
              if(substr($ext, 0, 6 ) === "title:") {
                $title = explode(':', $ext)[1];
                $description = $title;
              }
              if(substr($ext, 0, 6 ) === "description:") {
                $description = explode(':', $ext)[1];
              }
            }
        }
        return $this->fillStubForItemAndWrap('form-fields/carousel-stub.html', $item, array(
          'slide' => $slide_script,
          'title' => $title,
          'description' => $description,
          'viewName' => $this->viewName,
          'routeGroup' => $this->routeGroup
        ));
    }

    protected function createSlider($item)
    {
      return $this->fillStubForItemAndWrap('form-fields/slider-stub.html', $item, array());
    }



    protected function createCheckFeed($item)
    {

        return $this->fillStubForItemAndWrap('form-fields/checkfeed-stub.html', $item, array());
        /*
      $markup = $this->fillStub('form-fields/checkfeed-stub.html',
        array(
          'required' => $item['required'] ? 'required' : '',
          'itemName' => $item['name'],
          'crudNameSingular' => $this->crudNameSingular
        )
      );


        $start = $this->delimiter[0];
        $end = $this->delimiter[1];

        $required = $item['required'] ? 'required' : '';

        $markup = File::get($this->viewDirectoryPath . 'form-fields/checkfeed-stub.html');
        $markup = str_replace($start . 'required' . $end, $required, $markup);
        $markup = str_replace($start . 'itemName' . $end, $item['name'], $markup);
        $markup = str_replace($start . 'crudNameSingular' . $end, $this->crudNameSingular, $markup);

        return $this->wrapField(
            $item,
            $markup
        );
*/
    }


    protected function fillStubForItemAndWrap($stubFileName, $item, $elements) {

      $standardElements = array(
            'required' => $item['required'] ? 'required' : '',
            'fieldType' => $this->typeLookup[$item['type']],
            'itemName' => $item['name'],
            'crudNameSingular' => $this->crudNameSingular,
            'viewName' => $this->viewName,
            'routeGroup' => $this->routeGroup
      );

      $allElements = array_merge($standardElements, $elements);
      $markup = $this->fillStub($stubFileName, $allElements);

      return $this->wrapField(
          $item,
          $markup
      );

    }

    protected function fillStub($stubFileName, $elements)
    {
        $start = $this->delimiter[0];
        $end = $this->delimiter[1];

        if(File::Exists($this->customViewDirectoryPath . $stubFileName)) {
          $markup = "\n".File::get($this->customViewDirectoryPath . $stubFileName);

        } else {
          $markup = "\n".File::get($this->viewDirectoryPath . $stubFileName);
        }
        $markup = str_replace("\n", "\n                   ", $markup);


        foreach ($elements as $key => $value) {
          $markup = str_replace($start . $key . $end, $value, $markup);
        }

        return $markup;
    }

}
