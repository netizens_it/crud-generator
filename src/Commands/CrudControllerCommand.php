<?php

namespace gen11\CrudGenerator\Commands;

use Illuminate\Console\GeneratorCommand;
use File;

class CrudControllerCommand extends GeneratorCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'crud:controller
                            {name : The name of the controler.}
                            {--crud-name= : The name of the Crud.}
                            {--model-name= : The name of the Model.}
                            {--model-namespace= : The namespace of the Model.}
                            {--controller-namespace= : Namespace of the controller.}
                            {--view-path= : The name of the view path.}
                            {--fields= : Field names for the form & migration.}
                            {--validations= : Validation rules for the fields.}
                            {--route-group= : Prefix of the route group.}
                            {--file= : file for patches.}
                            {--pagination=25 : The amount of models per page for index pages.}
                            {--force : Overwrite already existing controller.}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new resource controller.';

    /**
     * The type of class being generated.
     *
     * @var string
     */
    protected $type = 'Controller';

    /**
     * Get the stub file for the generator.
     *
     * @return string
     */
    protected function getStub()
    {
        return config('crudgenerator.custom_template')
        ? config('crudgenerator.path') . '/controller.stub'
        : __DIR__ . '/../stubs/controller.stub';
    }

    /**
     * Get the default namespace for the class.
     *
     * @param  string $rootNamespace
     *
     * @return string
     */
    protected function getDefaultNamespace($rootNamespace)
    {
        return $rootNamespace . '\\' . ($this->option('controller-namespace') ? $this->option('controller-namespace') : 'Http\Controllers');
    }

    /**
     * Determine if the class already exists.
     *
     * @param  string  $rawName
     * @return bool
     */
    protected function alreadyExists($rawName)
    {
        if ($this->option('force')) {
            return false;
        }
        return parent::alreadyExists($rawName);
    }

    /**
     * Build the model class with the given name.
     *
     * @param  string  $name
     *
     * @return string
     */
    protected function buildClass($name)
    {
        $stub = $this->files->get($this->getStub());

        $viewPath = $this->option('view-path') ? $this->option('view-path') . '.' : '';
        $crudName = strtolower($this->option('crud-name'));
        $crudNameSingular = str_singular($crudName);
        $modelName = $this->option('model-name');
        $modelNamespace = $this->option('model-namespace');
        $routeGroup = ($this->option('route-group')) ? $this->option('route-group') . '/' : '';
        $routePrefix = ($this->option('route-group')) ? $this->option('route-group') : '';
        $routePrefixCap = ucfirst($routePrefix);
        $perPage = intval($this->option('pagination'));
        $viewName = snake_case($this->option('crud-name'), '-');
        $fields = $this->option('fields');
        $validations = rtrim($this->option('validations'), ';');
        $file = $this->option('file');

        $validationRules = '';
        if (trim($validations) != '') {
            $validationRules = "\$this->validate(\$request, [";

            $rules = explode(';', $validations);
            foreach ($rules as $v) {
                if (trim($v) == '') {
                    continue;
                }

                // extract field name and args
                $parts = explode('#', $v);
                $fieldName = trim($parts[0]);
                $rules = trim($parts[1]);
                $validationRules .= "\n\t\t\t'$fieldName' => '$rules',";
            }

            $validationRules = substr($validationRules, 0, -1); // lose the last comma
            $validationRules .= "\n\t\t]);";
        }

        $snippet = <<<EOD
        if (\$request->hasFile('{{fieldName}}')) {
                \$file = \$request->file('{{fieldName}}');
                \$uploadPath = public_path('/uploads/{{crudName}}/{{fieldName}}');
                \$extension = \$file->getClientOriginalExtension();
                \$fileName = date('Y-m-d-H-i-s') . '-' . rand(1, 999) . '.' . \$extension;

                \$file->move(\$uploadPath, \$fileName);
                \$requestData['{{fieldName}}'] = \$fileName;

        }
EOD;

        $fieldsArray = explode(';', $fields);
        $fileSnippet = '';
        $whereSnippet = '';
        $indexDataSnippet = '';
        $createDataSnippet = '';
        $editDataSnippet = '';
        $showDataSnippet = '';
        $ajaxRoutes = '';

        $useSnippet = '';

        $usedUse = array();
        array_push($usedUse, $modelName);


        $filters_navigation = "";

        if ($fields) {
            $x = 0;
            foreach ($fieldsArray as $index => $item) {

            //  echo "ITEM".$item."\n";

                $itemArray = explode('#', $item);


                if(trim($itemArray[1]) == 'token') {

                  //echo "TOKEN"."\n";
                  $createDataSnippet .= "'$itemArray[0]' =>  Uid::generate(),"."\n            ";
                  $editDataSnippet .= "'$itemArray[0]' =>  \$$crudNameSingular->$itemArray[0],"."\n            ";


                }

                if(trim($itemArray[1]) == 'filter') {

                  if($filters_navigation == '') {
                    $filters_navigation = ".'?";
                  }
                  else {
                    $filters_navigation .= "&";
                  }

                  $filters_navigation .= $itemArray[0]."='.\$object->".$itemArray[0];
                  $indexDataSnippet .= "'$itemArray[0]' =>  \$request['$itemArray[0]'],"."\n            ";
                  $createDataSnippet .= "'$itemArray[0]' =>  \$request['$itemArray[0]'],"."\n            ";
                  $editDataSnippet .= "'$itemArray[0]' =>  \$$crudNameSingular->$itemArray[0],"."\n            ";

                  echo $filters_navigation."\n\n";
                }





                if (trim($itemArray[1]) == 'file' || trim($itemArray[1]) == 'image') {
                    $tempfileSnippet = "\n\n" . str_replace('{{fieldName}}', trim($itemArray[0]), $snippet) . "\n";
                    $fileSnippet .= "\n\n" . str_replace('{{crudName}}',$crudName, $tempfileSnippet) . "\n";
                }




                $fieldName = trim($itemArray[0]);

                $whereSnippet .= ($index == 0) ? "where('$fieldName', 'LIKE', \"%\$keyword%\")" . "\n                " : "->orWhere('$fieldName', 'LIKE', \"%\$keyword%\")" . "\n                ";

                if (trim($itemArray[1]) == 'feed' || trim($itemArray[1]) == 'pad' || trim($itemArray[1]) == 'checkfeed' || trim($itemArray[1]) == 'carousel' || trim($itemArray[1]) == 'slider') {

                  $datainfo = array('', '', '');

                  $create = null;
                  $edit = null;
                  $show = null;

                  foreach($itemArray as $opt) {
                    $o = explode('=', $opt);
                    if($o[0] == 'data') {
                      $datainfo = explode('|', $o[1]);
                    }
                  }

                  if(!in_array($datainfo[0], $usedUse)) {
                    array_push($usedUse, $datainfo[0]);
                    $useSnippet .= "use App\\$datainfo[0];\n";
                  }

                  if(count($datainfo) == 3) {

                    if((trim($itemArray[1]) == 'carousel') || (trim($itemArray[1]) == 'slider')) {
                      $createDataSnippet .= "'$fieldName' =>  $datainfo[0]::All(),"."\n            ";
                      $editDataSnippet .= "'$fieldName' =>  $datainfo[0]::All(),"."\n            ";
                      $showDataSnippet .= "'$fieldName' =>  $datainfo[0]::All(),"."\n            ";
                    }
                    else {
                      $createDataSnippet .= "'$fieldName' =>  $datainfo[0]::All()->pluck('$datainfo[2]', '$datainfo[1]'),"."\n            ";
                      $editDataSnippet .= "'$fieldName' =>  $datainfo[0]::All()->pluck('$datainfo[2]', '$datainfo[1]'),"."\n            ";
                      $showDataSnippet .= "'$fieldName' =>  $datainfo[0]::All()->pluck('$datainfo[2]', '$datainfo[1]'),"."\n            ";
                    }
                  }
                  else {
                    $filter = explode(':', $datainfo[3]);
                    //$filter[0] = $datainfo[3];
                    //$filter[1] = $datainfo[4];


                    if((trim($itemArray[1]) == 'carousel') || (trim($itemArray[1]) == 'slider')) {
                      $createDataSnippet .= "'$fieldName' => $datainfo[0]::where('".$filter[0]."','=', \$request->old('".$filter[1]."'))->get(),"."\n            ";
                      $editDataSnippet .= "'$fieldName' =>  $datainfo[0]::where('".$filter[0]."','=', \$".$crudNameSingular."->".$filter[1].")->get(),"."\n            ";
                      $showDataSnippet .= "'$fieldName' => [],"."\n            ";
                      $ajaxRoutes .= "
        public function select_$filter[1]_for_$fieldName(Request \$request)
        {
          if(\$request->ajax()){
            \$data = $datainfo[0]::where('".$filter[0]."','=', \$request->".$filter[1].")->get();
          //  if(count(\$data) > 0)
              return view('/".$viewName."/select$datainfo[0]',compact('data'))->render();
            return null;
          }
        }";
                    }
                    else {
                      $createDataSnippet .= "'$fieldName' => $datainfo[0]::where('".$filter[0]."','=', \$request->old('".$filter[1]."'))->pluck('$datainfo[2]', '$datainfo[1]')->all(),"."\n            ";
                      $editDataSnippet .= "'$fieldName' =>  $datainfo[0]::where('".$filter[0]."','=', \$".$crudNameSingular."->".$filter[1].")->pluck('$datainfo[2]', '$datainfo[1]')->all(),"."\n            ";
                      $showDataSnippet .= "'$fieldName' => [],"."\n            ";
                      $ajaxRoutes .= "
        public function select_$filter[1]_for_$fieldName(Request \$request)
        {
          if(\$request->ajax()){
            \$data = $datainfo[0]::where('".$filter[0]."','=', \$request->".$filter[1].")->pluck('$datainfo[2]', '$datainfo[1]')->all();
          //  if(count(\$data) > 0)
              return view('/".$viewName."/select$datainfo[0]',compact('data'))->render();
            return null;
          }
        }";
                    }

                }






                }
            }

            $whereSnippet .= "->";
        }

        return $this->replaceNamespace($stub, $name)
            ->replaceViewPath($stub, $viewPath)
            ->replaceViewName($stub, $viewName)
            ->replaceCrudName($stub, $crudName)
            ->replaceCrudNameSingular($stub, $crudNameSingular)
            ->replaceModelName($stub, $modelName)
            ->replaceModelNamespace($stub, $modelNamespace)
            ->replaceModelNamespaceSegments($stub, $modelNamespace)
            ->replaceRouteGroup($stub, $routeGroup)
            ->replaceRoutePrefix($stub, $routePrefix)
            ->replaceRoutePrefixCap($stub, $routePrefixCap)
            ->replaceValidationRules($stub, $validationRules)
            ->replacePaginationNumber($stub, $perPage)
            ->replaceFileSnippet($stub, $fileSnippet)
            ->replaceWhereSnippet($stub, $whereSnippet)
            ->replaceCreateDataSnippet($stub, $createDataSnippet)
            ->replaceShowDataSnippet($stub, $showDataSnippet)
            ->replaceEditDataSnippet($stub, $editDataSnippet)
            ->replaceIndexDataSnippet($stub, $indexDataSnippet)
            ->replaceUseSnippet($stub, $useSnippet)
            ->replaceFiltersNavigationSnippet($stub, $filters_navigation)
            ->replacePatches($stub, $file)
            ->replaceAjaxRoutes($stub, $ajaxRoutes)
            ->replaceClass($stub, $name);

    }

    /**
     * Replace the viewName fo the given stub.
     *
     * @param string $stub
     * @param string $viewName
     *
     * @return $this
     */
    protected function replaceViewName(&$stub, $viewName)
    {
        $stub = str_replace('{{viewName}}', $viewName, $stub);

        return $this;
    }

    /**
     * Replace the viewPath for the given stub.
     *
     * @param  string  $stub
     * @param  string  $viewPath
     *
     * @return $this
     */
    protected function replaceViewPath(&$stub, $viewPath)
    {
        $stub = str_replace('{{viewPath}}', $viewPath, $stub);

        return $this;
    }

    /**
     * Replace the crudName for the given stub.
     *
     * @param  string  $stub
     * @param  string  $crudName
     *
     * @return $this
     */
    protected function replaceCrudName(&$stub, $crudName)
    {
        $stub = str_replace('{{crudName}}', $crudName, $stub);

        return $this;
    }

    /**
     * Replace the crudNameSingular for the given stub.
     *
     * @param  string  $stub
     * @param  string  $crudNameSingular
     *
     * @return $this
     */
    protected function replaceCrudNameSingular(&$stub, $crudNameSingular)
    {
        $stub = str_replace('{{crudNameSingular}}', $crudNameSingular, $stub);

        return $this;
    }

    /**
     * Replace the modelName for the given stub.
     *
     * @param  string  $stub
     * @param  string  $modelName
     *
     * @return $this
     */
    protected function replaceModelName(&$stub, $modelName)
    {
        $stub = str_replace('{{modelName}}', $modelName, $stub);

        return $this;
    }

    /**
     * Replace the modelNamespace for the given stub.
     *
     * @param  string  $stub
     * @param  string  $modelNamespace
     *
     * @return $this
     */
    protected function replaceModelNamespace(&$stub, $modelNamespace)
    {
        $stub = str_replace('{{modelNamespace}}', $modelNamespace, $stub);

        return $this;
    }

    /**
     * Replace the modelNamespace segments for the given stub
     *
     * @param $stub
     * @param $modelNamespace
     *
     * @return $this
     */
    protected function replaceModelNamespaceSegments(&$stub, $modelNamespace)
    {
        $modelSegments = explode('\\', $modelNamespace);
        foreach ($modelSegments as $key => $segment) {
            $stub = str_replace('{{modelNamespace[' . $key . ']}}', $segment, $stub);
        }

        $stub = preg_replace('{{modelNamespace\[\d*\]}}', '', $stub);

        return $this;
    }

    /**
     * Replace the routePrefix for the given stub.
     *
     * @param  string  $stub
     * @param  string  $routePrefix
     *
     * @return $this
     */
    protected function replaceRoutePrefix(&$stub, $routePrefix)
    {
        $stub = str_replace('{{routePrefix}}', $routePrefix, $stub);

        return $this;
    }

    /**
     * Replace the routePrefixCap for the given stub.
     *
     * @param  string  $stub
     * @param  string  $routePrefixCap
     *
     * @return $this
     */
    protected function replaceRoutePrefixCap(&$stub, $routePrefixCap)
    {
        $stub = str_replace('{{routePrefixCap}}', $routePrefixCap, $stub);

        return $this;
    }

    /**
     * Replace the routeGroup for the given stub.
     *
     * @param  string  $stub
     * @param  string  $routeGroup
     *
     * @return $this
     */
    protected function replaceRouteGroup(&$stub, $routeGroup)
    {
        $stub = str_replace('{{routeGroup}}', $routeGroup, $stub);

        return $this;
    }

    /**
     * Replace the validationRules for the given stub.
     *
     * @param  string  $stub
     * @param  string  $validationRules
     *
     * @return $this
     */
    protected function replaceValidationRules(&$stub, $validationRules)
    {
        $stub = str_replace('{{validationRules}}', $validationRules, $stub);

        return $this;
    }

    /**
     * Replace the pagination placeholder for the given stub
     *
     * @param $stub
     * @param $perPage
     *
     * @return $this
     */
    protected function replacePaginationNumber(&$stub, $perPage)
    {
        $stub = str_replace('{{pagination}}', $perPage, $stub);

        return $this;
    }

    /**
     * Replace the file snippet for the given stub
     *
     * @param $stub
     * @param $fileSnippet
     *
     * @return $this
     */
    protected function replaceFileSnippet(&$stub, $fileSnippet)
    {
        $stub = str_replace('{{fileSnippet}}', $fileSnippet, $stub);

        return $this;
    }

    /**
     * Replace the where snippet for the given stub
     *
     * @param $stub
     * @param $whereSnippet
     *
     * @return $this
     */
    protected function replaceWhereSnippet(&$stub, $whereSnippet)
    {
        $stub = str_replace('{{whereSnippet}}', $whereSnippet, $stub);

        return $this;
    }

    protected function replaceAjaxRoutes(&$stub, $ajaxRoutes)
    {
        $stub = str_replace('{{ajaxRoutes}}', $ajaxRoutes, $stub);

        return $this;
    }

    protected function replaceCreateDataSnippet(&$stub, $dataSnippet)
    {
        $stub = str_replace('{{createDataSnippet}}', $dataSnippet, $stub);

        return $this;
    }

    protected function replaceShowDataSnippet(&$stub, $dataSnippet)
    {
        $stub = str_replace('{{showDataSnippet}}', $dataSnippet, $stub);

        return $this;
    }

    protected function replaceEditDataSnippet(&$stub, $dataSnippet)
    {
        $stub = str_replace('{{editDataSnippet}}', $dataSnippet, $stub);

        return $this;
    }

    protected function replaceIndexDataSnippet(&$stub, $dataSnippet)
    {
        $stub = str_replace('{{indexDataSnippet}}', $dataSnippet, $stub);

        return $this;
    }

    protected function replaceUseSnippet(&$stub, $useSnippet)
    {
        $stub = str_replace('{{useSnippet}}', $useSnippet, $stub);

        return $this;
    }

    protected function replaceFiltersNavigationSnippet(&$stub, $filterSnippet)
    {
        $stub = str_replace('{{filtersNavigation}}', $filterSnippet, $stub);

        return $this;
    }



    protected function replacePatches(&$stub, $file)
    {

      $patchesToApply = ['controller_use', 'controller_store_pre', 'controller_store_post',
                            'controller_update', 'controller_update_pre', 'controller_update_post', 'controller_delete', 'controller_delete_pre', 'controller_delete_post'];

      $json = File::get($file);
      $jsonfile = json_decode($json);

      if (property_exists($jsonfile, 'patches')) {

                  foreach ($jsonfile->patches as $patch) {

                  if(isset($patch->code) && $patch->code != "") {
                      echo "applying patch: ".$patch->name."\n";
                      $patch->code = str_replace(";", ";\n\t",$patch->code);
                      $patch->code = "\n  // **** start ".$patch->name." patch\n\t ".$patch->code."// **** end ".$patch->name." patch\n\n";

                  }
                  $stub = str_replace('{{'.$patch->name.'}}', $patch->code, $stub);
                  if (($key = array_search($patch->name, $patchesToApply)) !== false) {
                      unset($patchesToApply[$key]);
                  }
                }
      }

      foreach ($patchesToApply as $patch) {
        echo "clearing patch: ".$patch."\n";;
        $stub = str_replace('{{'.$patch.'}}', '', $stub);
      }

    //    $stub = str_replace('{{relationships}}', '', $stub);
        return $this;
    }

}
